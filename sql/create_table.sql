CREATE TABLE `conygre`.`customers` (
                                       `customer_id` INT NOT NULL AUTO_INCREMENT,
                                       `first_name` VARCHAR(45) NOT NULL,
                                       `last_name` VARCHAR(45) NOT NULL,
                                       `city` VARCHAR(45) NOT NULL,
                                       `zip_code` VARCHAR(45) NOT NULL,
                                       `date_joined` DATE NOT NULL,
                                       PRIMARY KEY (`customer_id`));


INSERT INTO `conygre`.`customers` (`customer_id`, `first_name`, `last_name`, `city`, `zip_code`, `date_joined`) VALUES ('1', 'Hamza', 'Gharbieh', 'Dallas', '75040', '2021-07-09');
INSERT INTO `conygre`.`customers` (`customer_id`, `first_name`, `last_name`, `city`, `zip_code`, `date_joined`) VALUES ('2', 'Ceasar', 'Valle', 'Mesa', '85204', '2021-01-01');
INSERT INTO `conygre`.`customers` (`customer_id`, `first_name`, `last_name`, `city`, `zip_code`, `date_joined`) VALUES ('3', 'Nick', 'Todd', 'London', 'EC1A', '2000-10-29');


CREATE TABLE `conygre`.`quotes` (
                                    `quoteId` INT NOT NULL AUTO_INCREMENT,
                                    `customer_id` INT NULL,
                                    `dateBecameCustomer` DATE NULL,
                                    `price` DOUBLE NOT NULL,
                                    `quoteDate` DATE NOT NULL,
                                    `email` VARCHAR(45) NOT NULL,
                                    `startDate` DATE NOT NULL,
                                    `endDate` DATE NOT NULL,
                                    `vin` VARCHAR(45) NOT NULL,
                                    PRIMARY KEY (`quoteId`),
                                    INDEX `customer_id_idx` (`customer_id` ASC) VISIBLE,
                                    CONSTRAINT `customer_id`
                                        FOREIGN KEY (`customer_id`)
                                            REFERENCES `conygre`.`customers` (`customer_id`)
                                            ON DELETE NO ACTION
                                            ON UPDATE NO ACTION);





INSERT INTO `conygre`.`quotes` (`quoteId`, `customer_id`, `dateBecameCustomer`, `price`, `quoteDate`, `email`, `startDate`, `endDate`, `vin`) VALUES ('100', '1', '2021-07-09', '300.00', '2021-04-01', 'hamza@gmail.com', '2021-05-01', '2021-11-01', '2376yh8s74k9ruf74');
INSERT INTO `conygre`.`quotes` (`quoteId`, `dateBecameCustomer`, `price`, `quoteDate`, `email`, `startDate`, `endDate`, `vin`) VALUES ('101', '2', '150.65', '2021-12-30', 'random@yahoo.com', '2022-02-27', '2023-02-27', '7654hjs8dhc64us93');
insert into `conygre`.`quotes` VALUES ('103', NULL, NULL, '453', '2022-09-21', 'random2@yahoo.com', '2022-10-01', '2023-04-17', 'y8tu9ieu84huie85r')