package com.conygre.springrestproject.repo;


import com.conygre.springrestproject.entities.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
@Repository
public interface CustomerRepo extends JpaRepository<Customer, Integer> {

    //to read customers in database use http://localhost:8080/api/customers,
    // http://localhost:8080/api/customers/idNumnber
    public Iterable<Customer> findByFirstName(@Param("first_name")String firstName);
}
